DROP TABLE IF EXISTS product;

CREATE TABLE product (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  code VARCHAR(20) NOT NULL,
  description VARCHAR(250) NOT NULL,
  deleted bit DEFAULT 0
);

INSERT INTO product (code, description, deleted) VALUES
  ('bike01', 'trek marlin 7', 0),
  ('bike02', 'trek procaliber 9.7', 0),
  ('bike03', 'specialized stumpjumper', 0),
  ('bike04', 'trinx x1', 0),
  ('bike05', 'trinx x3', 0),
  ('bike07', 'trinx x7', 0),
  ('bike08', 'scott aspect 2021', 0),
  ('bike09', 'giant talon 2021', 0),
  ('bike10', 'specialized rockhopper 2021', 0),
  ('bike11', 'KHS 2021', 0);