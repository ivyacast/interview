package com.iyc.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

@RestControllerAdvice
public class ResponseExceptionHandler {

  @ExceptionHandler(Exception.class)
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  public final ExceptionResponse handleAllExceptions(Exception ex, WebRequest request){
    return new ExceptionResponse(LocalDateTime.now(), ex.getMessage(), request.getDescription(false));
  }

  @ExceptionHandler(ProductNotFoundException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  public final ExceptionResponse productNotFound(ProductNotFoundException ex, WebRequest request){
    return new ExceptionResponse(LocalDateTime.now(), ex.getMessage(), request.getDescription(false));
  }
}
