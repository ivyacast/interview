package com.iyc.controller.api;

import com.iyc.dto.Criteria;
import com.iyc.dto.PageObject;
import com.iyc.exception.MyResourceNotFoundException;
import com.iyc.exception.ProductNotFoundException;
import com.iyc.model.Product;
import com.iyc.service.ProductService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {
    private ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ResponseEntity<List<Product>> getAllProducts() {
        List<Product> productList = this.productService.getProducts();
        return new ResponseEntity<>(productList, HttpStatus.OK);
    }

    @GetMapping("not-deleted")
    public ResponseEntity<List<Product>> getNotDeletedProducts() {
        List<Product> productList = this.productService.getNotDeletedProducts();
        return new ResponseEntity<>(productList, HttpStatus.OK);
    }

    @GetMapping("/pageable")
    public ResponseEntity<PageObject<Product>> getPaginatedProducts(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size) {
        Pageable paging = PageRequest.of(page, size);
        PageObject<Product> resultPageProduct = this.productService.getProductsPageable(paging);
        if (page > resultPageProduct.getTotalPages()) {
            throw new MyResourceNotFoundException();
        }
        return new ResponseEntity<>(resultPageProduct, HttpStatus.OK);
    }

    @GetMapping("/pageable/filter")
    public ResponseEntity<PageObject<Product>> getFilteredPaginatedProducts(
            @RequestParam String description,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size) {
        Pageable paging = PageRequest.of(page, size);
        PageObject<Product> resultPageProduct;
        resultPageProduct = this.productService.getProductsByDescription(description, paging);
        if (page > resultPageProduct.getTotalPages()) {
            throw new MyResourceNotFoundException("Page resource not found");
        }
        return new ResponseEntity<>(resultPageProduct, HttpStatus.OK);
    }

    @GetMapping("/pageable/criteria")
    public ResponseEntity<List<Product>> getProductsByCriteria(@RequestBody Criteria criteria) {
        List<Product> productList = this.productService.searchByCriteria(criteria);
        return new ResponseEntity<>(productList, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Product> saveProduct(@RequestBody Product product) {
        Product newProduct = this.productService.save(product);
        return new ResponseEntity<>(newProduct, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Product> updateProduct(@RequestBody Product product) {
        Product updateProduct = this.productService.update(product);
        return new ResponseEntity<>(updateProduct, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable("id") Integer id) {
        Product product = this.productService.getProductById(id);
        if (product.getId() == null) {
            throw new ProductNotFoundException("ID NOT FOUND " + id);
        }
        this.productService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/logical-delete/{id}")
    public ResponseEntity<?> logicalDeleteProduct(@PathVariable("id") Integer id) {
        Product product = this.productService.getProductById(id);
        if (product.getId() == null) {
            throw new ProductNotFoundException("ID NOT FOUND " + id);
        }
        this.productService.logicalDeleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
