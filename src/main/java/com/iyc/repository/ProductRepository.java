package com.iyc.repository;

import com.iyc.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer>, ProductRepositoryCustom {
    @Modifying
    @Query("UPDATE Product p SET p.deleted = 1 WHERE id = ?1")
    void logicalDeleteById(Integer id);

    @Query("SELECT p FROM Product p WHERE p.deleted = 0")
    List<Product> getNotDeletedProducts();

    Page<Product> findByDescriptionContaining(String description, Pageable pageable);
}
