package com.iyc.repository.impl;

import com.iyc.dto.Criteria;
import com.iyc.dto.Filter;
import com.iyc.model.CustomPredicate;
import com.iyc.model.Product;
import com.iyc.repository.ProductRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class ProductRepositoryCustomImpl implements ProductRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Product> searchByCriteria(Criteria criteria) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> query = cb.createQuery(Product.class);
        Root<Product> productRoot = query.from(Product.class);
        List<Filter> filterList = criteria.getFilterList();
        List<CustomPredicate> customPredicateList = new ArrayList<>();
        filterList.forEach(filter -> {
            Path<String> fieldPath = productRoot.get(filter.getField());
            Predicate predicate = cb.like(fieldPath, "%" + filter.getValue() + "%");
            customPredicateList.add(new CustomPredicate(predicate, filter.getOperator()));
        });
        customPredicateList.forEach(cp -> {
            if (cp.getOperatorPredicate().equalsIgnoreCase("AND")) {
                query.where(cb.and(cp.getPredicate()));
            }
        });
        return entityManager.createQuery(query).setFirstResult(criteria.getPage()).setMaxResults(criteria.getSize())
                .getResultList();
    }
}
