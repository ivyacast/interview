package com.iyc.repository;

import com.iyc.dto.Criteria;
import com.iyc.model.Product;

import java.util.List;

public interface ProductRepositoryCustom {
    List<Product> searchByCriteria(Criteria criteria);
}
