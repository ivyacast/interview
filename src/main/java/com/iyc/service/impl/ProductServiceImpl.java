package com.iyc.service.impl;

import com.iyc.dto.Criteria;
import com.iyc.dto.PageObject;
import com.iyc.model.Product;
import com.iyc.repository.ProductRepository;
import com.iyc.service.ProductService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getProducts() {
        return this.productRepository.findAll();
    }

    @Override
    public List<Product> getNotDeletedProducts() {
        return this.productRepository.getNotDeletedProducts();
    }

    @Override
    public Product getProductById(Integer id) {
        Optional<Product> product = this.productRepository.findById(id);
        return product.orElseGet(Product::new);
    }

    @Override
    public PageObject<Product> getProductsPageable(Pageable pageable) {
        Page<Product> pageProduct = this.productRepository.findAll(pageable);
        return new PageObject<>(pageProduct.getContent(), pageProduct.getNumber(), pageProduct.getTotalElements(), pageProduct.getTotalPages());
    }

    @Override
    public PageObject<Product> getProductsByDescription(String description, Pageable pageable) {
        Page<Product> pageProduct = this.productRepository.findByDescriptionContaining(description, pageable);
        return new PageObject<>(pageProduct.getContent(), pageProduct.getNumber(), pageProduct.getTotalElements(), pageProduct.getTotalPages());
    }

    @Override
    public List<Product> searchByCriteria(Criteria criteria) {
        return this.productRepository.searchByCriteria(criteria);
    }

    @Override
    @Transactional
    public Product save(Product product) {
        return this.productRepository.save(product);
    }

    @Override
    @Transactional
    public Product update(Product product) {
        return this.productRepository.save(product);
    }

    @Override
    public void deleteById(Integer id) {
        this.productRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void logicalDeleteById(Integer id) {
        this.productRepository.logicalDeleteById(id);

    }
}
