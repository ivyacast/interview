package com.iyc.service;

import com.iyc.dto.Criteria;
import com.iyc.dto.PageObject;
import com.iyc.model.Product;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductService {
    List<Product> getProducts();

    List<Product> getNotDeletedProducts();

    Product getProductById(Integer id);

    PageObject<Product> getProductsPageable(Pageable pageable);

    PageObject<Product> getProductsByDescription(String title, Pageable pageable);

    List<Product> searchByCriteria(Criteria criteria);

    Product save(Product product);

    Product update(Product product);

    void deleteById(Integer id);

    void logicalDeleteById(Integer id);
}
