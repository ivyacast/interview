package com.iyc.model;

import javax.persistence.criteria.Predicate;

public class CustomPredicate {
    private Predicate predicate;
    private String operatorPredicate;

    public CustomPredicate(Predicate predicate, String operatorPredicate) {
        this.predicate = predicate;
        this.operatorPredicate = operatorPredicate;
    }

    public Predicate getPredicate() {
        return predicate;
    }

    public void setPredicate(Predicate predicate) {
        this.predicate = predicate;
    }

    public String getOperatorPredicate() {
        return operatorPredicate;
    }

    public void setOperatorPredicate(String operatorPredicate) {
        this.operatorPredicate = operatorPredicate;
    }
}
