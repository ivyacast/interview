package com.iyc;

import com.iyc.model.Product;
import com.iyc.repository.ProductRepository;
import com.iyc.service.ProductService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
class RestApiApplicationTests {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    ProductRepository repository;

    Product buildProduct(String code, String description, boolean deleted) {
        Product p1 = new Product();
        p1.setCode(code);
        p1.setDescription(description);
        p1.setDeleted(deleted);
        return p1;
    }

    @Test
    void should_find_products() {
        Iterable<Product> products = repository.findAll();
        assertThat(products).isNotEmpty();
    }

    @Test
    public void should_find_product_by_id() {
        Product p1 = buildProduct("bike100", "bike 100", false);
        entityManager.persist(p1);
        Product p2 = buildProduct("bike101", "bike 101", false);
        entityManager.persist(p2);

        Product foundProduct = repository.findById(p2.getId()).get();

        assertThat(foundProduct).isEqualTo(p2);
    }

	@Test
	public void should_find_product_by_description() {
		String description = "trek";
		Pageable paging = PageRequest.of(0, 10);

		Page<Product> pagingProducts = repository.findByDescriptionContaining(description, paging);
        Iterable<Product> products = pagingProducts.getContent();
		assertThat(products).hasSize(2);
	}

    @Test
    public void should_find_products_by_pagination() {
        String description = "trek";
        int page = 0;
        int size = 3;
        int totalSize = repository.findAll().size();

        Pageable paging = PageRequest.of(page, size);
        Page<Product> pagingProducts = repository.findAll(paging);

        assertThat(pagingProducts.getNumber()).isEqualTo(page);
        assertThat(pagingProducts.getSize()).isEqualTo(size);
        assertThat(pagingProducts.getTotalElements()).isEqualTo(totalSize);
    }

    @Test
    public void should_save_a_product() {
        Product p1 = buildProduct("bike100", "bike 100", false);
        Product newProduct = repository.save(p1);

        assertThat(newProduct).hasFieldOrPropertyWithValue("code", "bike100");
        assertThat(newProduct).hasFieldOrPropertyWithValue("description", "bike 100");
        assertThat(newProduct).hasFieldOrPropertyWithValue("deleted", false);
    }

    @Test
    public void should_update_product_by_id() {
		Product p1 = buildProduct("bike100", "bike 100", false);
        entityManager.persist(p1);

		Product p2 = buildProduct("bike101", "bike 101", false);
        entityManager.persist(p2);

		Product updatedProduct = buildProduct("bike101 upd", "bike 101 upd", false);;

		Product updatedProduct2 = repository.findById(p2.getId()).get();
		updatedProduct2.setCode(updatedProduct.getCode());
		updatedProduct2.setDescription(updatedProduct.getDescription());
		updatedProduct2.setDeleted(updatedProduct.getDeleted());
		repository.save(updatedProduct2);

		Product checkProduct = repository.findById(p2.getId()).get();

        assertThat(checkProduct.getId()).isEqualTo(updatedProduct2.getId());
        assertThat(checkProduct.getCode()).isEqualTo(updatedProduct.getCode());
        assertThat(checkProduct.getDescription()).isEqualTo(updatedProduct.getDescription());
        assertThat(checkProduct.getDeleted()).isEqualTo(updatedProduct.getDeleted());
    }

	@Test
	public void should_delete_product_by_id() {
		Product p1 = buildProduct("bike100", "bike 100", false);
		entityManager.persist(p1);

		Product p2 = buildProduct("bike101", "bike 101", false);
		entityManager.persist(p2);

		repository.deleteById(p2.getId());

		Iterable<Product> products = repository.findAll();

		assertThat(products).hasSize(11).contains(p1);
	}

	@Test
	public void should_logical_delete_product_by_id() {
    	//Total 10 products
		Product p1 = buildProduct("bike100", "bike 100", false);
		entityManager.persist(p1);

		Product p2 = buildProduct("bike101", "bike 101", false);
		entityManager.persist(p2);

		repository.logicalDeleteById(p2.getId());

		Iterable<Product> products = repository.getNotDeletedProducts();

		assertThat(products).hasSize(11).contains(p1);
	}
}
